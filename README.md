To build user-api html run thr following command  
```npm run bundle-user-api```  
Access docs/user-api/index.html to see the documentation.  

To run the server locally  
```rpm run serve-user-api```  
Access localhost 9090 to see the documentation.

